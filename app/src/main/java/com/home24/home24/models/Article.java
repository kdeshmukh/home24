package com.home24.home24.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.home24.home24.interfaces.ModelListener;
import com.home24.home24.utils.Constants;
import com.home24.home24.utils.Urls;
import com.rainingclouds.hatchttp.HatcHttpJSONListener;
import com.rainingclouds.hatchttp.HatcHttpRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Article model to hold the data coming from server.
 */
public class Article implements Parcelable {

    /**
     * TAG for logging
     */
    private static final String TAG = "###Article";

    private String mTitle;
    private String mSku;
    private String mImageUrl;
    private boolean mIsLiked;

    private Article(final Parcel in) {
        mTitle = in.readString();
        mSku = in.readString();
        mImageUrl = in.readString();
        mIsLiked = in.readByte() !=0;
    }

    public Article(){

    }

    public static final Creator<Article> CREATOR = new Creator<Article>() {
        @Override
        public Article createFromParcel(Parcel in) {
            return new Article(in);
        }

        @Override
        public Article[] newArray(int size) {
            return new Article[size];
        }
    };

    public static void getArticles(final int offset, final ModelListener<ArrayList<Article>> listener) {
        HatcHttpRequest.GET(Urls.ARTICLE_URL)
                .addHeader(Constants.HEADER_KEY, Constants.HEADER_VALUE)
                .addParam(Constants.PARAM_APP_DOMAIN_KEY, Constants.PARAM_APP_DOMAIN_VALUE)
                .addParam(Constants.PARAM_LIMIT_KEY, Constants.PARAM_LIMIT_KEY_VALUE)
                .addParam(Constants.PARAM_OFFSET_KEY, String.valueOf(offset))
                .execute(new HatcHttpJSONListener() {
                    @Override
                    public void onComplete(int status, JSONObject jsonObject) {
                        try {
                            final JSONArray articlesArray = jsonObject.getJSONObject("_embedded").getJSONArray("articles");
                            if (articlesArray == JSONObject.NULL) {
                                listener.onFailure(new Throwable("No articles found!"));
                                return;
                            }
                            final ArrayList<Article> list = new ArrayList<>();
                            for (int arrayIndex = 0; arrayIndex < articlesArray.length(); ++arrayIndex) {
                                Article article = new Article();
                                article.setTitle(articlesArray.getJSONObject(arrayIndex).getString("title"));
                                article.setSku(articlesArray.getJSONObject(arrayIndex).getString("sku"));
                                article.setImageUrl(articlesArray.getJSONObject(arrayIndex).getJSONArray("media").getJSONObject(0).getString("uri"));
                                list.add(article);
                            }
                            listener.onSuccess(status, list);
                        } catch (JSONException e) {
                            listener.onFailure(e);
                        }
                    }

                    @Override
                    public void onException(Throwable throwable) {
                        listener.onFailure(throwable);
                    }
                });
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mTitle);
        dest.writeString(mSku);
        dest.writeString(mImageUrl);
        dest.writeByte((byte) (mIsLiked ? 1 : 0));
    }


    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public String getSku() {
        return mSku;
    }

    public void setSku(String sku) {
        mSku = sku;
    }

    public String getImageUrl() {
        return mImageUrl;
    }

    public void setImageUrl(String imageUrl) {
        mImageUrl = imageUrl;
    }

    public boolean isLiked(){
        return mIsLiked;
    }

    public void setIsLiked(final boolean isLiked){
        mIsLiked = isLiked;
    }
}
