package com.home24.home24.adapters;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.home24.home24.R;
import com.home24.home24.models.Article;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by kaustubh on 22/08/15.
 */
public class ArticleAdapter extends BaseAdapter {

    private Context mContext;
    private ArrayList<Article> mData;

    private int mType;

    public ArticleAdapter(final Context context,
                          final int type,
                          final ArrayList<Article> data) {

        mContext = context;
        mType = type;
        mData = data;
    }

    @Override
    public int getCount() {
        return mData == null ? 0 : mData.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return mType == 1 ? getListItemView(convertView, position, parent)
                : getGridItemView(convertView, position, parent);
    }

    private View getListItemView(View convertView, int position, ViewGroup parent){
        ListViewHolder holder;
        if(convertView == null){
            holder = new ListViewHolder();
            convertView = LayoutInflater.from(mContext).inflate(R.layout.item_article_list, parent, false);
            holder.articleImage = (ImageView) convertView.findViewById(R.id.image);
            holder.articleText = (TextView) convertView.findViewById(R.id.text);
            holder.like = (ImageView) convertView.findViewById(R.id.like);
            convertView.setTag(holder);
        } else {
            holder = (ListViewHolder) convertView.getTag();
        }
        final Article article = mData.get(position);
        if(article.isLiked()){
            holder.like.setBackground(mContext.getResources().getDrawable(R.drawable.like));
        } else {
            holder.like.setBackground(mContext.getResources().getDrawable(R.drawable.dislike));
        }
        Picasso.with(mContext).load(article.getImageUrl()).into(holder.articleImage);
        holder.articleText.setText(article.getTitle());
        return convertView;
    }

    private View getGridItemView(View convertView, int position, ViewGroup parent){
        GridViewHolder holder;
        if(convertView == null){
            holder = new GridViewHolder();
            convertView = LayoutInflater.from(mContext).inflate(R.layout.item_article_grid, parent, false);
            holder.articleImage = (ImageView) convertView.findViewById(R.id.image);
            holder.like = (ImageView) convertView.findViewById(R.id.like);
            convertView.setTag(holder);
        } else {
            holder = (GridViewHolder) convertView.getTag();
        }
        final Article article = mData.get(position);
        Picasso.with(mContext).load(article.getImageUrl()).into(holder.articleImage);
        if(article.isLiked()){
            holder.like.setBackground(mContext.getResources().getDrawable(R.drawable.like));
        } else {
            holder.like.setBackground(mContext.getResources().getDrawable(R.drawable.dislike));
        }
        return convertView;
    }

    private class ListViewHolder {
        ImageView articleImage;
        TextView articleText;
        ImageView like;
    }

    private class GridViewHolder {
        ImageView articleImage;
        ImageView like;
    }
}
