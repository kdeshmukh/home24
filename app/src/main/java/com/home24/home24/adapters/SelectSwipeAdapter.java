package com.home24.home24.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.home24.home24.R;
import com.home24.home24.models.Article;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Adapter for swipe view, used in SelectionActivity
 */
public class SelectSwipeAdapter extends BaseAdapter {

    private Context mContext;
    private ArrayList<Article> mData;

    public SelectSwipeAdapter(final Context context){
        mContext = context;
    }

    @Override
    public int getCount() {
        return mData == null ? 0 : mData.size();
    }

    @Override
    public Article getItem(int position) {
        return mData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if(convertView == null){
            holder = new ViewHolder();
            convertView = LayoutInflater.from(mContext).inflate(R.layout.item_adapterview, parent, false);
            holder.articleImage = (ImageView) convertView.findViewById(R.id.card_image);
            holder.articleText = (TextView) convertView.findViewById(R.id.card_text);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        final Article article = getItem(position);
        Picasso.with(mContext).load(article.getImageUrl()).into(holder.articleImage);
        holder.articleText.setText(article.getTitle());
        return convertView;
    }

    private class ViewHolder {
        ImageView articleImage;
        TextView articleText;
    }

    public void setData(final ArrayList<Article> data) {
        mData = data;
        notifyDataSetChanged();
    }
}
