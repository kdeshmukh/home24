package com.home24.home24.interfaces;

/**
 * listeners for communication
 */
public interface ModelListener<T> {
    void onSuccess(final int status, final T object);
    void onFailure(final Throwable throwable);
}
