package com.home24.home24.views.activities.base;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

/**
 * Base activity, we will be keeping all the reusable
 * functions here
 */
public class BaseActivity extends AppCompatActivity  {

    /**
     * Context instance to hold application context
     */
    protected Context mContext;

    /**
     * Handler instance to hold handler instance.
     * This will help us to run code on main thread.
     * Note : We will be initializing handler only on main thread.
     */
    protected Handler mHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    /**
     * Helper method to set action bar up as true or false
     */
    protected void setHomeAsEnabled(final boolean homeAsEnabled){
        if(getSupportActionBar() == null)
            return;

        getSupportActionBar().setDisplayHomeAsUpEnabled(homeAsEnabled);
    }

    /**
     * Helper method to hide actionbar
     */
    protected void hideActionBar(){
        if(getSupportActionBar() == null)
            return;

        getSupportActionBar().hide();
    }

    /**
     * Helper method to change actionbar color
     * @param color
     */
    protected void makeChangeActionBarColor(final int color){
        if(getSupportActionBar() == null)
            return;

        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(color));
    }

    /**
     * Helper method to set the actionbar title
     * @param title
     */
    protected void changeActionBarTitle(final String title){
        if(getSupportActionBar() == null)
            return;

        getSupportActionBar().setTitle(title);
    }
}
