package com.home24.home24.views.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.home24.home24.R;
import com.home24.home24.models.Article;
import com.home24.home24.utils.Constants;
import com.home24.home24.utils.Intents;
import com.home24.home24.views.activities.base.BaseActivity;
import com.home24.home24.views.fragments.ArticleFragment;

import java.util.ArrayList;

public class ReviewActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_review);

        final ArrayList<Article> list = getIntent()
                .getBundleExtra(Constants.BUNDLE)
                .getParcelableArrayList(Constants.ARTICLES);

        setHomeAsEnabled(true);
        makeChangeActionBarColor(getResources().getColor(R.color.actionbar_color));
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.activity_review_container,
                        ArticleFragment.getInstance(list),
                        ArticleFragment.TAG).commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_review, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        switch (id){
            case R.id.action_switch:
                Intent intent = new Intent(Constants.SWITCH);
                sendBroadcast(intent);
                break;

            case android.R.id.home:
                startActivity(Intents.getSelectionIntent());
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        startActivity(Intents.getSelectionIntent());
        finish();
    }
}
