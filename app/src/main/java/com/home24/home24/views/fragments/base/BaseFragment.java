package com.home24.home24.views.fragments.base;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class BaseFragment extends DialogFragment {

    /**
     * Context instance to hold application context
     */
    protected Context mContext;

    /**
     * Handler instance to hold handler instance.
     * This will help us to run code on main thread.
     * Note : We will be initializing handler only on main thread.
     */
    protected Handler mHandler;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }
}
