package com.home24.home24.views.fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ListView;

import com.home24.home24.R;
import com.home24.home24.adapters.ArticleAdapter;
import com.home24.home24.models.Article;
import com.home24.home24.utils.Constants;
import com.home24.home24.views.fragments.base.BaseFragment;

import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicBoolean;


public class ArticleFragment extends BaseFragment {

    public static final String TAG = "ArticleFragment";

    private ListView mListView;
    private GridView mGridView;

    private AtomicBoolean mIsListShowing;
    private BroadcastReceiver mSwitchReceiver;
    private ArrayList<Article> mData;

    public ArticleFragment() {

    }

    public static ArticleFragment getInstance(final ArrayList<Article> data) {
        final Bundle arguments = new Bundle();
        final ArticleFragment articleFragment = new ArticleFragment();
        arguments.putParcelableArrayList(Constants.ARTICLES, data);
        articleFragment.setArguments(arguments);
        return articleFragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_articles, container, false);
        mContext = getActivity();
        mListView = (ListView) view.findViewById(R.id.list);
        mGridView = (GridView) view.findViewById(R.id.grid);
        mData = getArguments().getParcelableArrayList(Constants.ARTICLES);
        mIsListShowing = new AtomicBoolean(true);
        mListView.setAdapter(new ArticleAdapter(mContext, 1, mData));
        mGridView.setAdapter(new ArticleAdapter(mContext, 0, mData));

        mListView.setSelector(new ColorDrawable(getResources()
                .getColor(android.R.color.transparent)));
        mGridView.setSelector(new ColorDrawable(getResources()
                .getColor(android.R.color.transparent)));

        mSwitchReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                mIsListShowing.set(!mIsListShowing.get());
                if(mIsListShowing.get()) {
                    mListView.setVisibility(View.VISIBLE);
                    mGridView.setVisibility(View.INVISIBLE);
                } else {
                    mListView.setVisibility(View.INVISIBLE);
                    mGridView.setVisibility(View.VISIBLE);
                }
            }
        };
        getActivity().registerReceiver(mSwitchReceiver,
                new IntentFilter(Constants.SWITCH));

        return view;
    }


    @Override
    public void onDetach() {
        super.onDetach();
        mData = null;
        getActivity().unregisterReceiver(mSwitchReceiver);
    }
}
