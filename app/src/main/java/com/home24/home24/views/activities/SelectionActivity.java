package com.home24.home24.views.activities;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.home24.home24.R;
import com.home24.home24.adapters.SelectSwipeAdapter;
import com.home24.home24.interfaces.ModelListener;
import com.home24.home24.models.Article;
import com.home24.home24.utils.Constants;
import com.home24.home24.utils.Intents;
import com.home24.home24.views.activities.base.BaseActivity;
import com.lorentzos.flingswipe.SwipeFlingAdapterView;

import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Second activity that will be shown to user
 * after he clicks on START button from MainActivity
 *
 * Here articles fetched from server will be shown to user.
 * He can like/dislike them
 *
 * After liking/disliking 10 articles user should be able
 * to see the REVIEW button.
 */
public class SelectionActivity extends BaseActivity implements SwipeFlingAdapterView.onFlingListener {

    /**
     * TAG for logging
     */
    private static final String TAG = "###Selection";

    // variables to store count
    private int mTotalSelected;
    private int mTotalLiked;

    @Bind(R.id.swipe_view)
    SwipeFlingAdapterView mSwipeView;
    @Bind(R.id.selected)
    TextView mSelectedTextView;
    @Bind(R.id.like)
    ImageView mLike;
    @Bind(R.id.dislike)
    ImageView mDisLike;
    @OnClick(R.id.like)
    void like(){
        mSwipeView.getTopCardListener().selectLeft();
    }
    @OnClick(R.id.dislike)
    void dislike(){
        mSwipeView.getTopCardListener().selectRight();
    }
    @Bind(R.id.error_textview)
    TextView mErrorTextView;
    @Bind(R.id.main_container)
    RelativeLayout mMainContainer;
    @Bind(R.id.review)
    Button mReview;
    @OnClick(R.id.review)
    void review() {
        startActivity(Intents.getReviewIntent(mTraversedData));
        finish();
    }

    private SelectSwipeAdapter mSwipeAdapter;
    private AtomicInteger mOffset;
    private AtomicBoolean mIsFetching;
    private ArrayList<Article> mData;
    private ArrayList<Article> mTraversedData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_selection);
        mContext = getApplicationContext();
        ButterKnife.bind(this);
        /**
         * Create handler on main thread, so posting runnable on handler
         * will execute it on UI thread
         */
        mHandler = new Handler();
        mSwipeAdapter = new SelectSwipeAdapter(mContext);
        mData = new ArrayList<>();
        mTraversedData = new ArrayList<>();
        mSwipeView.setAdapter(mSwipeAdapter);
        mSwipeView.setFlingListener(this);
        mMainContainer.setVisibility(View.INVISIBLE);
        makeChangeActionBarColor(getResources().getColor(R.color.actionbar_color));
        setHomeAsEnabled(true);
        mOffset = new AtomicInteger(0);
        mIsFetching = new AtomicBoolean(false);
        getArticles();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == android.R.id.home) {
            startActivity(Intents.getHomeIntent());
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * function to get the articles from server
     */
    private void getArticles(){
        if(mIsFetching.get()){
            return;
        }
        mIsFetching.set(true);
        final ProgressDialog progressDialog = ProgressDialog.show(this, "" , getString(R.string.getting_articles));
        progressDialog.setCancelable(false);
        Article.getArticles(mOffset.get(), new ModelListener<ArrayList<Article>>() {
            @Override
            public void onSuccess(final int status, final ArrayList<Article> articles) {
                Log.d(TAG , " List of articles " + articles.size());
                /**
                 * here handler will help us to execute code on UI thread
                 */
                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        progressDialog.dismiss();
                        mErrorTextView.setVisibility(View.INVISIBLE);
                        mMainContainer.setVisibility(View.VISIBLE);
                        mData.addAll(articles);
                        mSwipeAdapter.setData(mData);
                        mDisLike.setVisibility(View.VISIBLE);
                        mLike.setVisibility(View.VISIBLE);
                        mOffset.set(mOffset.get() + Constants.LIMIT);
                        mIsFetching.set(false);
                    }
                });
            }

            @Override
            public void onFailure(final Throwable throwable) {
                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        progressDialog.dismiss();
                        mErrorTextView.setVisibility(View.VISIBLE);
                        mErrorTextView.setText(throwable.getMessage());
                        mMainContainer.setVisibility(View.INVISIBLE);
                    }
                });
            }
        });
    }

    private void removeItem(final boolean isLiked) {
        Log.d(TAG, "Total size " + mData.size());
        Article article = mData.get(0);
        if(isLiked) {
            mTotalLiked += 1;
            article.setIsLiked(true);
        } else {
            article.setIsLiked(false);
        }
        mTraversedData.add(article);
        mData.remove(0);
        mSwipeAdapter.setData(mData);
        mTotalSelected += 1;
        mSelectedTextView.setText("Total likes " + mTotalLiked + "/" + mTotalSelected);
        if(mTotalSelected >= Constants.LIMIT) {
            mReview.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void removeFirstObjectInAdapter() {

    }

    @Override
    public void onLeftCardExit(Object o) {
        mSwipeView.getTopCardListener().selectLeft();
        removeItem(true);
    }

    @Override
    public void onRightCardExit(Object o) {
        mSwipeView.getTopCardListener().selectRight();
        removeItem(false);
    }

    @Override
    public void onAdapterAboutToEmpty(int i) {
        getArticles();
    }

    @Override
    public void onScroll(float v) {
        View view = mSwipeView.getSelectedView();
        view.findViewById(R.id.like_indicator).setAlpha(v < 0 ? -v : 0);
        view.findViewById(R.id.dislike_indicator).setAlpha(v > 0 ? v : 0);
    }
}
