package com.home24.home24.views.activities;

import android.os.Bundle;

import com.home24.home24.R;
import com.home24.home24.utils.Intents;
import com.home24.home24.views.activities.base.BaseActivity;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends BaseActivity {

    /**
     * ButterKnife will handle everything for us :)
     */
    @OnClick(R.id.start)
    void startNextActivity(){
        startActivity(Intents.getSelectionIntent());
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        /**
         * Bind the view, so that we can use them
         */
        ButterKnife.bind(this);
        hideActionBar();
    }
}
