package com.home24.home24.utils;

/**
 * As we will never be initializing this class,
 * better make it as final. It will boost the performance.
 */
public final class Constants {
    public static final String HEADER_KEY = "Accept-Language";
    public static final String HEADER_VALUE = "de_DE";
    public static final String PARAM_APP_DOMAIN_KEY = "appDomain";
    public static final String PARAM_APP_DOMAIN_VALUE = "1";
    public static final String PARAM_LIMIT_KEY = "limit";
    public static final String PARAM_OFFSET_KEY = "offset";
    public static final String PARAM_LIMIT_KEY_VALUE = "10";
    public static final String ARTICLES = "articles";
    public static final String BUNDLE = "bundle";
    public static final String SWITCH = "switch";
    public static final String SWITCH_FLAG = "switch_flag";

    public static final int LIMIT = 10;
}
