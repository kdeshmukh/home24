package com.home24.home24.utils;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import com.home24.home24.models.Article;

import java.util.ArrayList;

/**
 * Intents custom class, to get the respective intents of the
 * activities. Instead in using explicit intents, we will
 * use implicit. Making code much cleaner.
 */
public final class Intents {

    private static final String INTENT_PREFIX = "home24://android_app";

    private static final String SELECTION = "/select/";
    private static final String MAIN = "/main/";
    private static final String REVIEW = "/review/";

    public static Intent getSelectionIntent(){
        final Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(INTENT_PREFIX + SELECTION));
        return intent;
    }

    public static Intent getHomeIntent(){
        final Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(INTENT_PREFIX + MAIN));
        return intent;
    }

    public static Intent getReviewIntent(final ArrayList<Article> articles){
        final Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(INTENT_PREFIX + REVIEW));
        final Bundle bundle = new Bundle();
        bundle.putParcelableArrayList(Constants.ARTICLES, articles);
        intent.putExtra(Constants.BUNDLE, bundle);
        return intent;
    }
}
