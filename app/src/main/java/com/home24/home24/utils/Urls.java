package com.home24.home24.utils;

/**
 * As we will never be initializing this class,
 * better make it as final. It will boost the performance.
 *
 * This class will simply hold the urls which will be used in the app.
 */
public final class Urls {

    private static final String BASE_URL = "https://api-mobile.home24.com/api/v1";
    public static final String ARTICLE_URL = BASE_URL + "/articles";
}
